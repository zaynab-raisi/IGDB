import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

import PageGameIndex from "./pages/game/Index.vue";
import PageGameDetail from "./pages/game/GameDetail.vue";
import NotFound from "./pages/NotFound.vue";

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "games",
      component: PageGameIndex
    },
    {
      path: "/game/:id/detail",
      component: PageGameDetail,
      name: "game",
      meta: {
        title: "Page Game"
      }
    },
    {
      path: "*",
      name: "notFound",
      component: NotFound,
    }
  ]
});

export default router;
