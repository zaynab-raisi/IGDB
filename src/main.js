import Vue from 'vue';
import App from './App.vue';
import router from "./router";
import axios from 'axios';
import config from '../config';


// Element UI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/styles/main.scss';
Vue.use(ElementUI);

// Axios configs
axios.defaults.headers.common['user-key'] = config.MY_KEY;
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

// Proxy config 
// Adding as a global property
Vue.prototype.$proxyurl = 'https://cors-anywhere.herokuapp.com/';

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')
